package uz.smart.kapital.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.smart.kapital.entity.Order;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResCustomerMax {
    private Integer id;
    private String name;
    private String country;
    private String address;
    private String phone;
    private Date date;
}
