package uz.smart.kapital.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.dao.DataAccessException;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResOrder {
    private Integer id;
    private Date date;
    private Integer customerId;
}
