package uz.smart.kapital.projection;

public interface ResHighDemandProduct {
    Integer getCode();

    Double getTotalOrder();
}
