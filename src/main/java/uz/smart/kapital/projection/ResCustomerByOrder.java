package uz.smart.kapital.projection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


public interface ResCustomerByOrder {
    Integer getId();

    String getName();

    Date getLastOrderDate();
}
