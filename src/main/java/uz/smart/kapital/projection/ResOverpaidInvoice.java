package uz.smart.kapital.projection;

public interface ResOverpaidInvoice {
    Integer getNumber();

    Double getAmount();
}
