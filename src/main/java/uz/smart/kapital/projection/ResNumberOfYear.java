package uz.smart.kapital.projection;

public interface ResNumberOfYear {
    Integer getTotalOrder();

    String getCountry();
}
