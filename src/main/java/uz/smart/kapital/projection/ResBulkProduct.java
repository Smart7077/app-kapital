package uz.smart.kapital.projection;

public interface ResBulkProduct {

    Integer getCode();

    Double getPrice();
}
