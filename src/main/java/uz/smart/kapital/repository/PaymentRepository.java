package uz.smart.kapital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.smart.kapital.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {
}
