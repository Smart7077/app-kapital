package uz.smart.kapital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.smart.kapital.entity.Category;
import uz.smart.kapital.projection.CustomCategory;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
