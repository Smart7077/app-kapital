package uz.smart.kapital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.smart.kapital.entity.Order;
import uz.smart.kapital.payload.ApiResponse;
import uz.smart.kapital.payload.ReqOrderDetail;
import uz.smart.kapital.repository.OrderRepository;
import uz.smart.kapital.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    OrderService orderService;
    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/{order_id}")
    public HttpEntity<?> getOrderList(@PathVariable Integer order_id) {

        return ResponseEntity.ok(orderService.getOrderById(order_id));
    }

    @GetMapping("/withoutDetil")
    public HttpEntity<?> getOrder() {
        return ResponseEntity.ok(orderService.getOrderWithoutDetil());
    }

    @PostMapping
    public HttpEntity<?> addOrder(@RequestBody ReqOrderDetail reqOrderDetail) {
        ApiResponse response = orderService.addOrder(reqOrderDetail);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/number_of_products_in_year")
    public HttpEntity<?> getTotalNumber() {
        return ResponseEntity.ok(orderRepository.getTotalOrderInYear());
    }

    @GetMapping("/orders_without_invoices")
    public HttpEntity<?> getOrdersWithoutInvoice() {
        return ResponseEntity.ok(orderRepository.getOrderWithoutInvoice());
    }

}
