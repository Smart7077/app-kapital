package uz.smart.kapital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.smart.kapital.entity.Category;
import uz.smart.kapital.entity.Product;
import uz.smart.kapital.repository.CategoryRepository;
import uz.smart.kapital.repository.ProductRepository;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ProductRepository productRepository;

    @GetMapping
    public HttpEntity<?> getCategoryList() {
        return ResponseEntity.ok(categoryRepository.findAll());
    }


    @GetMapping("{product_id}")
    public HttpEntity<?> getCategoryById(@PathVariable Integer product_id) {
        Product product = productRepository.findById(product_id).orElseThrow(() -> new ResourceNotFoundException("getProduct"));
        Category category = categoryRepository.findById(product.getCategory().getId()).orElseThrow(() -> new ResourceNotFoundException("getCategory"));
        return ResponseEntity.ok(category);
    }
}
