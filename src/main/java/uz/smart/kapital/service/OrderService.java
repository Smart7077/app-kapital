package uz.smart.kapital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import uz.smart.kapital.entity.Detail;
import uz.smart.kapital.entity.Order;
import uz.smart.kapital.entity.Product;
import uz.smart.kapital.payload.ApiResponse;
import uz.smart.kapital.payload.ReqOrderDetail;
import uz.smart.kapital.payload.ResOrder;
import uz.smart.kapital.payload.ResOrderProduct;
import uz.smart.kapital.repository.CustomerRepository;
import uz.smart.kapital.repository.DetailRepository;
import uz.smart.kapital.repository.OrderRepository;
import uz.smart.kapital.repository.ProductRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    DetailRepository detailRepository;
    @Autowired
    ProductRepository productRepository;

    public List<ResOrder> getOrderWithoutDetil() {
        List<Order> orderList = orderRepository.getOrdersWithOutDetail();
        return orderList.stream().map(this::getOrder).collect(Collectors.toList());
    }

    public ResOrder getOrder(Order order) {
        return new ResOrder(
                order.getId(),
                order.getDate(),
                order.getCustomer().getId()
        );
    }

    public ApiResponse addOrder(ReqOrderDetail reqOrderDetail) {
        try {
            Order order = new Order();
            order.setDate(new Date());
            order.setCustomer(customerRepository.getOne(reqOrderDetail.getCustomer_id()));
            Order savedOrder = orderRepository.save(order);
            Detail detail = new Detail();
            detail.setOrder(orderRepository.getOne(savedOrder.getId()));
            detail.setProduct(productRepository.getOne(reqOrderDetail.getProduct_id()));
            detail.setQuantity(reqOrderDetail.getQuantity());
            Detail savedDetail = detailRepository.save(detail);
            return new ApiResponse("SUCCESS", true, savedDetail.getId());
        } catch (Exception e) {
            return new ApiResponse("FAILED", false);
        }
    }

    public ResOrderProduct getOrderById(Integer id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getOrder"));
        Detail detail = detailRepository.findByOrderId(order.getId());
        Product product = productRepository.findById(detail.getProduct().getId()).orElseThrow(() -> new ResourceNotFoundException("getProduct"));

        return new ResOrderProduct(
                order.getId(),
                order.getDate(),
                order.getCustomer().getId(),
                product.getName()
        );
    }
}
